import moment from 'moment';

const initialYear = 3136;
const firstDay = moment([1970, 0, 1]);

const apostleMap = {
    "Chaos" : "Mungday",
    "Discord" : "Mojoday",
    "Confusion" : "Syaday",
    "Bureaucracy" : "Zaraday",
    "Aftermath" : "Maladay"
};
const seasonMap = {
    "Chaos" : "Chaoflux",
    "Discord" : "Discoflux",
    "Confusion" : "Confuflux",
    "Bureaucracy" : "Bureflux",
    "Aftermath" : "Afflux"
};
const weekdayMap = [
    "Sweetmorn",
    "Boomtime",
    "Pungenday",
    "Prickle-Prickle",
    "Setting Orange"
];

let seasonMemo = {};

class Season {
    constructor(name, year, monthIndex, dateNum, options) {
        this.initialDay = options && options.initialDay || 1;
        this.name = name;
        this.date = moment([year, monthIndex, dateNum]);
    }
}
function seasonFactory(year) {
    let result;
    let memo = seasonMemo[year];
    if(memo instanceof Array) {
        result = memo;
    } else {
        let chaos = new Season("Chaos", year, 0, 1);
        let moreChaos = new Season("Chaos", year, 2, 1, { initialDay : 60 });
        let discord = new Season("Discord", year, 2, 15);
        let confusion = new Season("Confusion", year, 4, 27);
        let bureaucracy = new Season("Bureaucracy", year, 7, 8);
        let aftermath = new Season("Aftermath", year, 9, 20);

        seasonMemo[year] = result =
            [chaos, moreChaos, discord, confusion, bureaucracy, aftermath];
    }

    return result;
}

class DiscordianDate {
    /**
     *  Create a Discordian date corresponding to the passed-in date.
     *  @param {Date} date - A Date object.
     *  @param {string} date - A date string.
     *  @param {number} date - An epoch time.
     *  @prop {number} year - The current year.
     *  @prop {string} season - The name of the current season.
     *  @prop {number} day - The current day of the season.
     *  @prop {number} weekdayIndex - The current day of the week as an integer.
     *  @prop {string} weekday - The current day of the week.
     *  @prop {string} holyday - The current holyday, if any.
     */
    constructor(date) {
        if(date === undefined) {
            date = new Date();
        } else if(Number.isFinite(date) || date.length !== undefined) {
            date = new Date(date);
        } else if (!(date instanceof Date)) {
            throw new RangeError("Bad argument passed to DiscordianDate constructor.");
        }

        let currYear = date.getYear() + 1900;
        let now = moment([currYear, date.getMonth(), date.getDate()]);
        let seasons = seasonFactory(currYear);

        this.year = initialYear + currYear - 1970;

        let weekdayIndex = now.diff(firstDay, 'days');
        this.weekdayIndex = weekdayIndex % weekdayMap.length;
        //  Ensure pre-1970 dates also get a weekday
        this.weekdayIndex = this.weekdayIndex >= 0 ? this.weekdayIndex : (this.weekdayIndex + weekdayMap.length) % weekdayMap.length;

        if(now.date() === 29 && now.month() === 1) {
            // St. Tib's Day
            this.season = "Chaos";
            this.day = 59.5;
            this.holyday = "St. Tib's Day";
        } else {
            let index;
            for(
                index = 0;
                index < seasons.length - 1 && now.diff(seasons[index + 1].date, 'days') >= 0;
                index++
            );

            let season = seasons[index];
            this.season = season.name;

            let dayNumber = season.initialDay + now.diff(season.date, 'days');
            this.day = dayNumber;

            let holyday = '';
            if(dayNumber === 5) {
                holyday = apostleMap[season.name];
            } else if(dayNumber === 50) {
                holyday = seasonMap[season.name];
            }
            this.holyday = holyday;
        }
    }

    /**
     *  Returns the Discordian date string for the present day.
     *  @return {string} The current Discordian date as a string.
     */
    static today() {
        return (new DiscordianDate()).toString();
    }

    get weekday() {
        return weekdayMap[this.weekdayIndex];
    }

    /**
     *  Returns the string representation of the Discordian date.
     *  @return {string} This object's Discordian date string.
     */
    toString() {
        return `${
            this.holyday ? this.holyday + ', ' : ''
        }${this.weekday} ${this.season} ${this.day} ${this.year}`;
    }
}

export default DiscordianDate;
