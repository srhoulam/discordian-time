<a name="DiscordianDate"></a>
## DiscordianDate
**Kind**: global class  
**Properties**

| Name | Type | Description |
| --- | --- | --- |
| year | <code>number</code> | The current year. |
| season | <code>string</code> | The name of the current season. |
| day | <code>number</code> | The current day of the season. |
| weekdayIndex | <code>number</code> | The current day of the week as an integer. |
| weekday | <code>string</code> | The current day of the week. |
| holyday | <code>string</code> | The current holyday, if any. |


* [DiscordianDate](#DiscordianDate)
    * [new DiscordianDate(date)](#new_DiscordianDate_new)
    * _instance_
        * [.toString()](#DiscordianDate+toString) ⇒ <code>string</code>
    * _static_
        * [.today()](#DiscordianDate.today) ⇒ <code>string</code>

<a name="new_DiscordianDate_new"></a>
### new DiscordianDate(date)
Create a Discordian date corresponding to the passed-in date.


| Param | Type | Description |
| --- | --- | --- |
| date | <code>Date</code> | A Date object. |
| date | <code>string</code> | A date string. |
| date | <code>number</code> | An epoch time. |

<a name="DiscordianDate+toString"></a>
### discordianDate.toString() ⇒ <code>string</code>
Returns the string representation of the Discordian date.

**Kind**: instance method of <code>[DiscordianDate](#DiscordianDate)</code>  
**Returns**: <code>string</code> - This object's Discordian date string.  
<a name="DiscordianDate.today"></a>
### DiscordianDate.today() ⇒ <code>string</code>
Returns the Discordian date string for the present day.

**Kind**: static method of <code>[DiscordianDate](#DiscordianDate)</code>  
**Returns**: <code>string</code> - The current Discordian date as a string.  

---

# [Live demo](https://srhoulam.github.io/discordian-date)
