'use strict';

var DDate = require('../');

describe("Discordian date", function() {
    describe("is correct in 2016 on", function() {
        it("Chaos 1 / Jan 1", function() {
            var d = new Date();
            d.setMonth(0);
            d.setDate(1);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Chaos");
            expect(dd.day).toBe(1);
            expect(dd.holyday).toBe('');
        });
        it("Chaos 5 / Jan 5", function() {
            var d = new Date();
            d.setMonth(0);
            d.setDate(5);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Mungday");
        });
        it("St. Tib's day / Feb 29", function() {
            var d = new Date();
            d.setMonth(1);
            d.setDate(29);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Chaos");
            expect(dd.day).toBe(59.5);
            expect(dd.holyday).toBe("St. Tib's Day");

        });
        it("Chaos 50 / Mar 19", function() {
            var d = new Date();
            d.setMonth(1);
            d.setDate(19);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Chaoflux");
        });
        it("Chaos 60 / Mar 1", function() {
            var d = new Date();
            d.setMonth(2);
            d.setDate(1);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Chaos");
            expect(dd.day).toBe(60);
            expect(dd.holyday).toBe('');
        });
        it("Chaos 73 / Mar 14", function() {
            var d = new Date();
            d.setMonth(2);
            d.setDate(14);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Chaos");
            expect(dd.day).toBe(73);
            expect(dd.holyday).toBe('');
        });
        it("Discord 1 / Mar 15", function() {
            var d = new Date();
            d.setMonth(2);
            d.setDate(15);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Discord");
            expect(dd.day).toBe(1);
            expect(dd.holyday).toBe('');
        });
        it("Discord 5 / Mar 19", function() {
            var d = new Date();
            d.setMonth(2);
            d.setDate(19);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Mojoday");
        });
        it("Discord 50 / May 3", function() {
            var d = new Date();
            d.setMonth(4);
            d.setDate(3);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Discoflux");
        });
        it("Discord 73 / May 26", function() {
            var d = new Date();
            d.setMonth(4);
            d.setDate(26);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Discord");
            expect(dd.day).toBe(73);
            expect(dd.holyday).toBe('');
        });
        it("Confusion 1 / May 27", function() {
            var d = new Date();
            d.setMonth(4);
            d.setDate(27);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Confusion");
            expect(dd.day).toBe(1);
            expect(dd.holyday).toBe('');
        });
        it("Confusion 5 / May 31", function() {
            var d = new Date();
            d.setMonth(4);
            d.setDate(31);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Syaday");
        });
        it("Confusion 50 / Jul 15", function() {
            var d = new Date();
            d.setMonth(6);
            d.setDate(15);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Confuflux");
        });
        it("Confusion 73 / Aug 7", function() {
            var d = new Date();
            d.setMonth(7);
            d.setDate(7);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Confusion");
            expect(dd.day).toBe(73);
            expect(dd.holyday).toBe('');
        });
        it("Bureaucracy 1 / Aug 8", function() {
            var d = new Date();
            d.setMonth(7);
            d.setDate(8);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Bureaucracy");
            expect(dd.day).toBe(1);
            expect(dd.holyday).toBe('');
        });
        it("Bureaucracy 5 / Aug 12", function() {
            var d = new Date();
            d.setMonth(7);
            d.setDate(12);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Zaraday");
        });
        it("Bureaucracy 50 / Sep 26", function() {
            var d = new Date();
            d.setMonth(8);
            d.setDate(26);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Bureflux");
        });
        it("Bureaucracy 73 / Oct 19", function() {
            var d = new Date();
            d.setMonth(9);
            d.setDate(19);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Bureaucracy");
            expect(dd.day).toBe(73);
            expect(dd.holyday).toBe('');
        });
        it("Aftermath 1 / Oct 20", function() {
            var d = new Date();
            d.setMonth(9);
            d.setDate(20);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Aftermath");
            expect(dd.day).toBe(1);
            expect(dd.holyday).toBe('');
        });
        it("Aftermath 5 / Oct 24", function() {
            var d = new Date();
            d.setMonth(9);
            d.setDate(24);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Maladay");
        });
        it("Aftermath 50 / Dec 8", function() {
            var d = new Date();
            d.setMonth(11);
            d.setDate(8);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Afflux");
        });
        it("Aftermath 73 / Dec 31", function() {
            var d = new Date();
            d.setMonth(11);
            d.setDate(31);
            d.setYear(2016);

            var dd = new DDate(d);
            expect(dd.season).toBe("Aftermath");
            expect(dd.day).toBe(73);
            expect(dd.holyday).toBe('');
        });
    });
    describe("is correct in 2016 on", function() {
        it("Chaos 1 / Jan 1", function() {
            var d = new Date();
            d.setMonth(0);
            d.setDate(1);

            var dd = new DDate(d);
            expect(dd.season).toBe("Chaos");
            expect(dd.day).toBe(1);
            expect(dd.holyday).toBe('');
        });
        it("Chaos 5 / Jan 5", function() {
            var d = new Date();
            d.setMonth(0);
            d.setDate(5);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Mungday");
        });
        it("St. Tib's day / Feb 29", function() {
            var d = new Date();
            d.setMonth(1);
            d.setDate(29);

            var dd = new DDate(d);
            expect(dd.season).toBe("Chaos");
            expect(dd.day).toBe(59.5);
            expect(dd.holyday).toBe("St. Tib's Day");

        });
        it("Chaos 50 / Mar 19", function() {
            var d = new Date();
            d.setMonth(1);
            d.setDate(19);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Chaoflux");
        });
        it("Chaos 60 / Mar 1", function() {
            var d = new Date();
            d.setMonth(2);
            d.setDate(1);

            var dd = new DDate(d);
            expect(dd.season).toBe("Chaos");
            expect(dd.day).toBe(60);
            expect(dd.holyday).toBe('');
        });
        it("Chaos 73 / Mar 14", function() {
            var d = new Date();
            d.setMonth(2);
            d.setDate(14);

            var dd = new DDate(d);
            expect(dd.season).toBe("Chaos");
            expect(dd.day).toBe(73);
            expect(dd.holyday).toBe('');
        });
        it("Discord 1 / Mar 15", function() {
            var d = new Date();
            d.setMonth(2);
            d.setDate(15);

            var dd = new DDate(d);
            expect(dd.season).toBe("Discord");
            expect(dd.day).toBe(1);
            expect(dd.holyday).toBe('');
        });
        it("Discord 5 / Mar 19", function() {
            var d = new Date();
            d.setMonth(2);
            d.setDate(19);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Mojoday");
        });
        it("Discord 50 / May 3", function() {
            var d = new Date();
            d.setMonth(4);
            d.setDate(3);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Discoflux");
        });
        it("Discord 73 / May 26", function() {
            var d = new Date();
            d.setMonth(4);
            d.setDate(26);

            var dd = new DDate(d);
            expect(dd.season).toBe("Discord");
            expect(dd.day).toBe(73);
            expect(dd.holyday).toBe('');
        });
        it("Confusion 1 / May 27", function() {
            var d = new Date();
            d.setMonth(4);
            d.setDate(27);

            var dd = new DDate(d);
            expect(dd.season).toBe("Confusion");
            expect(dd.day).toBe(1);
            expect(dd.holyday).toBe('');
        });
        it("Confusion 5 / May 31", function() {
            var d = new Date();
            d.setMonth(4);
            d.setDate(31);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Syaday");
        });
        it("Confusion 50 / Jul 15", function() {
            var d = new Date();
            d.setMonth(6);
            d.setDate(15);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Confuflux");
        });
        it("Confusion 73 / Aug 7", function() {
            var d = new Date();
            d.setMonth(7);
            d.setDate(7);

            var dd = new DDate(d);
            expect(dd.season).toBe("Confusion");
            expect(dd.day).toBe(73);
            expect(dd.holyday).toBe('');
        });
        it("Bureaucracy 1 / Aug 8", function() {
            var d = new Date();
            d.setMonth(7);
            d.setDate(8);

            var dd = new DDate(d);
            expect(dd.season).toBe("Bureaucracy");
            expect(dd.day).toBe(1);
            expect(dd.holyday).toBe('');
        });
        it("Bureaucracy 5 / Aug 12", function() {
            var d = new Date();
            d.setMonth(7);
            d.setDate(12);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Zaraday");
        });
        it("Bureaucracy 50 / Sep 26", function() {
            var d = new Date();
            d.setMonth(8);
            d.setDate(26);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Bureflux");
        });
        it("Bureaucracy 73 / Oct 19", function() {
            var d = new Date();
            d.setMonth(9);
            d.setDate(19);

            var dd = new DDate(d);
            expect(dd.season).toBe("Bureaucracy");
            expect(dd.day).toBe(73);
            expect(dd.holyday).toBe('');
        });
        it("Aftermath 1 / Oct 20", function() {
            var d = new Date();
            d.setMonth(9);
            d.setDate(20);

            var dd = new DDate(d);
            expect(dd.season).toBe("Aftermath");
            expect(dd.day).toBe(1);
            expect(dd.holyday).toBe('');
        });
        it("Aftermath 5 / Oct 24", function() {
            var d = new Date();
            d.setMonth(9);
            d.setDate(24);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Maladay");
        });
        it("Aftermath 50 / Dec 8", function() {
            var d = new Date();
            d.setMonth(11);
            d.setDate(8);

            var dd = new DDate(d);
            expect(dd.holyday).toBe("Afflux");
        });
        it("Aftermath 73 / Dec 31", function() {
            var d = new Date();
            d.setMonth(11);
            d.setDate(31);

            var dd = new DDate(d);
            expect(dd.season).toBe("Aftermath");
            expect(dd.day).toBe(73);
            expect(dd.holyday).toBe('');
        });
    });
});
